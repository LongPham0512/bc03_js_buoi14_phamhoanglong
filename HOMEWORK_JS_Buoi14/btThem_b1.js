// Viết chương trình nhập vào ngày, tháng, năm. Tìm ngày, tháng, năm của ngày tiếp theo. Tương tự tìm ngày tháng năm của ngày trước đó.
var NgayTiepTheo = function () {
  var day = document.getElementById("ngay").value * 1;
  var month = document.getElementById("thang").value * 1;
  var year = document.getElementById("nam").value * 1;
  console.log(day, month, year);
  var tomorrow = "";

  if (day < 0 || day > 31) {
    alert("Ngày không xác định");
  }

  if (month < 0 || month > 12) {
    alert("Tháng không xác định");
  }

  switch (month) {
    case 4: /**Những tháng 30 ngày */
    case 6:
    case 9:
    case 11:
      if (day == 30) {
        tomorrow = 1;
        month += 1;
      } else if (day < 30) {
        tomorrow = day + 1;
      } else if (day > 30) {
        alert("Ngày không xác định");
      }
      break;

    case 1: /**Những tháng 31 ngày */
    case 3:
    case 5:
    case 7:
    case 8:
    case 10:
    case 12:
      if (day == 31) {
        tomorrow = 1;
      } else if (day < 31) {
        tomorrow = day + 1;
      } else if (day > 31) {
        alert("Ngày không xác định");
      }

      if (day == 31 && month == 12) {
        tomorrow = 1;
        month = 1;
        year += 1;
      }
      break;

    case 2 /**Tháng 28 ngày */:
      if (day == 28) {
        tomorrow = 1;
        month = 3;
      } else if (day < 28) {
        tomorrow = day + 1;
      } else if (day > 28) {
        alert("Ngày không xác định");
      }
      break;

    default:
      "Nhập không hợp lệ";
  }
  console.log(tomorrow, month, year);
  document.getElementById(
    "ket_qua1p"
  ).innerHTML = `<p>Ngày Tiếp Theo: ${tomorrow}/ ${month}/ ${year}</p>`;
};

var NgayHomQua = function () {
  var day = document.getElementById("ngay").value * 1;
  var month = document.getElementById("thang").value * 1;
  var year = document.getElementById("nam").value * 1;
  console.log(day, month, year);
  var yesterday = "";

  if (day < 0 || day > 31) {
    alert("Ngày không xác định");
  }

  if (month < 0 || month > 12) {
    alert("Tháng không xác định");
  }

  switch (month) {
    case 2:
    case 4:
    case 6:
    case 9:
    case 11:
      if (day < 30 && day > 1) {
        yesterday = day - 1;
      } else if (day == 1) {
        yesterday = 31;
        month -= 1;
      } else if (day > 30) {
        alert("Ngày không xác định");
      }
      break;

    case 1:
    case 3:
    case 5:
    case 7:
    case 8:
    case 10:
    case 12:
      if (day <= 31 && day > 1) {
        yesterday = day - 1;
      } else if (day == 1 && month == 1) {
        yesterday = 31;
        month = 12;
        year -= 1;
      } else if (day == 1 && month == 3) {
        yesterday = 28;
        month = 2;
      } else if (day == 1 && month == 8) {
        /**tháng 7 có 31 ngày */
        yesterday = 31;
        month = 7;
      } else if (day == 1) {
        yesterday = 30;
        month -= 1;
      }
      break;

    default:
      "Nhập không hợp lệ";
  }
  console.log(yesterday, month, year);
  document.getElementById(
    "ket_qua1p"
  ).innerHTML = `<p>Ngày Hôm Qua: ${yesterday}/ ${month}/ ${year}</p>`;
};
