//Viết chương trình nhập vào tháng, năm. Cho biết tháng đó có bao nhiêu ngày. (bao gồm tháng của năm nhuận).
var tinhSoNgay = function () {
  var Thang = document.getElementById("thang_b2").value * 1;
  var Nam = document.getElementById("nam_b2").value * 1;
  console.log({ Thang, Nam });

  var SoNgayTrongThang = "";
  if (Thang < 1 || Thang > 12) {
    alert("NHẬP SAI GIÁ TRỊ");
  }
  if (Nam < 1) {
    alert("NHẬP SAI GIÁ TRỊ");
  }

  switch (Thang) {
    case 4:
    case 6:
    case 9:
    case 11:
      SoNgayTrongThang = 30;
      break;
    case 1:
    case 3:
    case 5:
    case 7:
    case 8:
    case 10:
    case 12:
      SoNgayTrongThang = 31;
      break;
    case 2:
      if ((Nam % 4 == 0 && Nam % 100 !== 0) || Nam % 400 == 0) {
        SoNgayTrongThang = 29;
      } else {
        SoNgayTrongThang = 28;
      }
    default:
      "Nhập không hợp lệ";
  }
  console.log({ SoNgayTrongThang });
  document.getElementById(
    "ket_qua2p"
  ).innerHTML = `Có ${SoNgayTrongThang} ngày trong tháng ${Thang} năm ${Nam}`;
};
