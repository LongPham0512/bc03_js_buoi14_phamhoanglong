// Viết chương trình cho nhập 3 cạnh của tam giác. Hãy cho biết đó là tam giác gì?

var xetTamGiac = function () {
  var a = document.getElementById("canhA").value * 1;
  var b = document.getElementById("canhB").value * 1;
  var c = document.getElementById("canhC").value * 1;
  var ketQua = null;

  console.log(a, b, c);
  if (a == b && a == c && b == c) {
    ketQua = "Tam giác đều";
  } else if (a == b || a == c || b == c) {
    ketQua = "Tam giác cân";
  } else if (
    a * a == b * b + c * c ||
    b * b == a * a + c * c ||
    c * c == a * a + b * b
  ) {
    ketQua = "Tam giác vuông";
  } else {
    ketQua = "Tam giác thường";
  }

  console.log(ketQua);

  document.getElementById(
    "ket_qua4"
  ).innerHTML = `<span> ĐÂY LÀ: ${ketQua}</span>`;
};
