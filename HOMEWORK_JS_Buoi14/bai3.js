// Cho 3 số nguyên. Viết chương trình xuất ra có bao nhiêu số lẻ và bao nhiêu số chẵn.

var inKetQua = function () {
  var n1 = document.getElementById("soNguyen1").value * 1;
  var n2 = document.getElementById("soNguyen2").value * 1;
  var n3 = document.getElementById("soNguyen3").value * 1;
  console.log(n1, n2, n3);

  var soChan = "";
  var soLe = "";

  if (n1 % 2 == 0) {
    soChan += n1 + " ";
  } else {
    soLe += n1 + " ";
  }

  if (n2 % 2 == 0) {
    soChan += n2 + " ";
  } else {
    soLe += n2 + " ";
  }

  if (n3 % 2 == 0) {
    soChan += n3 + " ";
  } else {
    soLe += n3 + " ";
  }

  console.log(soChan, soLe);

  document.getElementById("ket_qua3").innerHTML = `<p>Số Chẳn: ${soChan}</p> 
  <p>Số Lẻ: ${soLe}</p>`;
};
