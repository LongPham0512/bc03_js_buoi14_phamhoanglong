// Viết chương trình xuất 3 số theo thứ tự tăng dần

var xepSttTangDan = function () {
  var n1 = document.getElementById("so_nguyen1").value * 1;
  var n2 = document.getElementById("so_nguyen2").value * 1;
  var n3 = document.getElementById("so_nguyen3").value * 1;
  console.log(n1, n2, n3);

  var nMax1 = n1;
  var nMax2, nMax3;

  if (n2 > nMax1 && n2 > n3) {
    nMax1 = n2;
  } else if (n3 > nMax1 && n3 > n2) {
    nMax1 = n3;
  }
  console.log(nMax1);

  if (nMax1 > n1 && (n1 > n2 || n1 > n3)) {
    nMax2 = n1;
  } else if (nMax1 > n2 && (n2 > n1 || n2 > n3)) {
    nMax2 = n2;
  } else if (nMax1 > n3 && (n3 > n1 || n3 > n2)) {
    nMax2 = n3;
  }
  console.log(nMax2);

  if (n1 < nMax1 && n1 < nMax2) {
    nMax3 = n1;
  } else if (n2 < nMax1 && n2 < nMax2) {
    nMax3 = n2;
  } else if (n3 < nMax1 && n3 < nMax2) {
    nMax3 = n3;
  }
  console.log(nMax3);

  console.log(nMax3, nMax2, nMax1);

  document.getElementById(
    "ket_qua1"
  ).innerHTML = `<span>STT tăng dần: ${nMax3}, ${nMax2}, ${nMax1}</span>`;
};
